package lesson9.hw3;

import java.util.InputMismatchException;

public class User {

    String name;
    String surname;

    User () {
    }
    public void setName(String name) throws InputMismatchException {

        if (name.contains("0") ||
                name.contains("1") ||
                name.contains("2") ||
                name.contains("3") ||
                name.contains("4") ||
                name.contains("5") ||
                name.contains("6") ||
                name.contains("7") ||
                name.contains("8") ||
                name.contains("9")) {
            System.out.println("Данные введены неверно");
            throw new InputMismatchException();
        }
        this.name = name;
    }

    public void setSurname(String surname) throws InputMismatchException {

        if (surname.contains("0") ||
                surname.contains("1") ||
                surname.contains("2") ||
                surname.contains("3") ||
                surname.contains("4") ||
                surname.contains("5") ||
                surname.contains("6") ||
                surname.contains("7") ||
                surname.contains("8") ||
                surname.contains("9")) {
            System.out.println("Данные введены неверно");
            throw new InputMismatchException();
        }
        this.surname = surname;
    }

    public void getFullName() {
        System.out.println("Пользователь: " + name + " " + surname);
    }

}
