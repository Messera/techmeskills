package lesson9.hw1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Maths {

    public static void main(String[] args) {

        try {
            getCircleArea();
        } catch (InputMismatchException e1) {
            System.out.println("Вы ввели некорректное значение: " + e1.fillInStackTrace());
            System.out.println("Для вычисления площади круга введите корректное значение радиуса, " +
                    "иначе программа перейдет к вычислению площади прямоугольника.");
            try {
                getCircleArea();
            }
            catch (InputMismatchException e2) {
                System.out.println("Вы все же ввели некорректное значение...");
            }
        }

        finally {
            System.out.println("Программа по расчету площади круга завершена.");
        }

        System.out.println("");

        try {
            getRectangleArea();
        } catch (InputMismatchException e1) {
            System.out.println("Вы ввели некорректное значение.");
            System.out.println("Для вычисления площади прямоугольника введите корректное значение сторон, " +
                    "иначе программа завершится.");
            try {
                getRectangleArea();
            }
            catch (InputMismatchException e2) {
                System.out.println("Вы все же ввели некорректное значение...");
            }
        }

        finally {
            System.out.println("Программа по расчету площади прямоугольника завершена.");
        }

  }

    private static void getCircleArea() throws InputMismatchException {
        Scanner scanner = new Scanner(System.in);
        Circle circle = new Circle();
        System.out.println("Введите радиус круга: ");
        int r = scanner.nextInt();
        if (r <= 0) {
            throw new InputMismatchException();
        }
        circle.setRadius(r);
        System.out.println("Радиус круга = " + circle.getRadius());
        circle.getArea();
        circle.getPerimeter();
    }

    private static void getRectangleArea() throws InputMismatchException {
        Scanner scanner = new Scanner(System.in);
        Rectangle rectangle = new Rectangle();
        System.out.println("Введите ширину прямоугольника: ");
        int w = scanner.nextInt();
        if (w <= 0) {
            throw new InputMismatchException();
        }
        rectangle.setWidth(w);
        System.out.println("Введите высоту прямоугольника: ");
        int h = scanner.nextInt();
        if (h <= 0) {
            throw new InputMismatchException();
        }
        rectangle.setHeight(h);
        System.out.println("У нас есть прямоугольник со сторонами: " + rectangle.getWidth() + " и " + rectangle.getHeight());
        rectangle.getArea();
        rectangle.getPerimeter();
    }

}
