package lesson9.hw1;

public class Circle extends Shape {

    protected int radius;

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void getArea() {
        System.out.println("Площадь круга равна: " + Math.PI * (radius * radius));
    }

    @Override
    public void getPerimeter() {
        System.out.println("Длина окружности равна: " + Math.PI * 2 * radius);
    }
}
