package lesson9.hw1;

public abstract class Shape {

    public abstract void getArea();
    public abstract void getPerimeter();

}
