package lesson9.ex2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InputHandler {
    public static void main(String[] args) {
        try {
            int[] numbers = new int[5];
            Scanner scanner = new Scanner(System.in);

            for (int i = 0; i <= numbers.length; i++) {
                numbers[i] = scanner.nextInt();
                System.out.println("Полученное значение: " + numbers[i]);
            }
        } catch (ArrayIndexOutOfBoundsException X) {
            System.out.println("Проблема с индексом");

        } catch (InputMismatchException t) {
            System.out.println("Вы ввели что-то не то");
        } finally {
            System.out.println("Расчет окончен");
        }
    }

}

