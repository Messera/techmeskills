package lesson3;

import java.util.Scanner;

public class HW3 {

    public static void main(String[] args) {
        task4();
    }


    private static void task1() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество элементов массива:");
        int[] arr = new int[scanner.nextInt()];
        System.out.println("Введите элементы массива:");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
        }
        System.out.println("Заданный массив: ");
        for (int j : arr) {
            System.out.print(j + " ");
        }
        System.out.println("");

        //сортировка массива:
        for (int k = 0; k < arr.length; k++) {
            {
                for (int j = 0; j < arr.length - 1; j++)
                    if (arr[j] > arr[j + 1]) {
                        int tmp = arr[j + 1];
                        arr[j + 1] = arr[j];
                        arr[j] = tmp;
                    }
            }
        }


        System.out.println("Массив теперь отсортирован по возрастанию и выглядит так: ");
        for (int j : arr) {
            System.out.print(j + " ");
        }


    }

    private static void task2() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество элементов массива:");
        int[] arr = new int[scanner.nextInt()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 100);
        }
        System.out.println("Сгенерированный массив: ");
        for (int j : arr) {
            System.out.print(j + " ");
        }
        System.out.println("");

        for (int k = 0; k < arr.length; k++) {
            {
                for (int j = 0; j < arr.length - 1; j++)
                    if (arr[j] < arr[j + 1]) {
                        int tmp = arr[j + 1];
                        arr[j + 1] = arr[j];
                        arr[j] = tmp;
                    }
            }
        }
        System.out.println("Массив теперь отсортирован по убыванию и выглядит так: ");
        for (int j : arr) {
            System.out.print(j + " ");
        }

    }


    private static void task3() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Перед вами пример глупой сортировки.");
        System.out.println("Введите количество элементов массива:");
        int[] arr = new int[scanner.nextInt()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 100);
        }
        System.out.println("Сгенерированный массив: ");
        for (int k : arr) {
            System.out.print(k + " ");
        }
        System.out.println("");

        for (int j = 0; j < arr.length - 1; j++)
            if (arr[j] > arr[j + 1]) {
                int tmp = arr[j + 1];
                arr[j + 1] = arr[j];
                arr[j] = tmp;
                j = -1;
            }

        System.out.println("Массив теперь отсортирован по возрастанию, но глупо: ");
        for (int l : arr) {
            System.out.print(l + " ");
        }

    }

    private static void task4() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Программа найдет минимум в двумерном массиве.");
        System.out.println("Введите количество строк массива:");
        int line = scanner.nextInt();
        System.out.println("Введите количество столбцов массива:");
        int column = scanner.nextInt();
        int[][] arr = new int[line][column];
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < column; j++) {
                arr[i][j] = (int) (Math.random() * 100);
            }
        }
        System.out.println("Сгенерированный массив: ");
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < column; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println("");

        }
        int min = 2147483647;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < column; j++) {
                if (arr[i][j] < min) {
                    min = arr[i][j];
                }
            }
        }
        System.out.println("Минимальное значение двумерного массива: " + min);
    }

}
