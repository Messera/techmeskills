package lesson3;

import java.util.Scanner;

public class Main3 {

    public static void main(String[] args) {
        task2();
    }

    private static void task1() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество элементов массива:");
        int[] arr = new int[scanner.nextInt()];
        System.out.println("Введите элементы массива:");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
        }
        System.out.println("Массив теперь выглядит так: ");
        for (int j : arr) {
            System.out.print(j + " ");
        }


    }


    private static void task2() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество элементов массива:");
        int[] arr = new int[scanner.nextInt()];
        System.out.println("Введите элементы массива:");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
        }
        System.out.println("Массив теперь выглядит так: ");
        for (int j : arr) {
            System.out.print(j + " ");
        }
        System.out.println("");

        for (int k = 0; k < arr.length; k++) {
            {
                for (int j = 0; j < arr.length -1; j++)
                    if (arr[j]>arr[j+1]) {
                int tmp = arr[j+1];
                arr[j+1] = arr[j];
                arr[j] = tmp;

            }
        }}
        System.out.println("Массив теперь отсортирован и выглядит так: ");
        for (int j : arr) {
            System.out.print(j + " ");
        }



    }
}
