package lesson11;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class AverageNum {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        List<Integer> numbers = new LinkedList<>();
        System.out.println("Введите элементы массива. Чтобы прекратить ввод напишите STOP");

        // Через цикл for

//        for (int i = 0; ; i++) {
//            String n = scanner.next();
//            if (Objects.equals(n, "STOP"))
//                break;
//            numbers.add(i, Integer.valueOf(n));
//            }

        // Через while


        String n = scanner.next();
        while (!n.equals("STOP")) {

            try {
                numbers.add(Integer.valueOf(n));
            } catch (Exception e) {
                System.out.println("Вы ввели неправильное значение. Введите число, либо напишите STOP.");
            }

            n = scanner.next();
        }


        double sum = 0;
        for (Integer number : numbers) {
            sum += number;
        }
        System.out.println("Среднее арифметическое списка = " + sum / numbers.size());
    }

}


