package lesson11.exampleset;


import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class SampleSet {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите элементы массива. Чтобы прекратить ввод напишите STOP");

        String n = scanner.next();
        Set<Person> persons = new LinkedHashSet<>();

        while (!n.equals("STOP")) {
            Person person = new Person(n);
            persons.add(person);
            n = scanner.next();
        }

        for (Person j : persons) {
            System.out.println(j.getName());
        }

    }

}
