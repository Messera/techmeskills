package lesson11;

import java.util.HashMap;
import java.util.Map;

public class SampleUseMap {

    public static void main(String[] args) {
        Map<String, Integer> mapByStr = new HashMap<>();
        mapByStr.put("один", 1);
        mapByStr.put("два", 2);
        mapByStr.put("три", 3);

        System.out.println(mapByStr.get("три"));

        Map<Integer, String> mapByInt = new HashMap<>();
        mapByInt.put(1, "один");
        mapByInt.put(2, "два");
        mapByInt.put(3, "три");

        System.out.println(mapByInt.get(2));

    }

}
