package lesson11;

import java.util.HashSet;
import java.util.Set;

public class SampleUseSet {

    public static void main(String[] args) {

        Set<String> collection = new HashSet<>();
        collection.add("John");
        collection.add("Kate");
        collection.add("Kate");

        for (String n : collection) {
            System.out.println(n);
        }
    }

}
