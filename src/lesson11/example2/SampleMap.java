package lesson11.example2;


import java.util.*;

public class SampleMap {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Map<String, Person> persons = new HashMap<>();

        System.out.println("Введите имя человека. Чтобы прекратить ввод напишите STOP.");
        String name = scanner.next();
        System.out.println("Введите его телефон");
        String phone = scanner.next();

        while (!name.equals("STOP")) {
            Person person = new Person(name,phone);
            persons.put(name,person);
            System.out.println("Введите имя человека:");
            name = scanner.next();
            if (name.equals("STOP"))
                break;
            System.out.println("Введите его телефон:");
            phone = scanner.next();
        }

        for (Person j : persons.values()) {
            System.out.println(j.getName()+ " - " + j.getPhone());
        }

        System.out.println("Чей номер телефона вы хотите узнать?");
        System.out.println(persons.get(scanner.next()).getPhone());

    }

}
