package lesson11.hw3;

import lesson11.hw2.Person;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class HashStates {

    public static void main(String[] args) {
        Map<Integer, String> states = new HashMap<Integer, String>();
        states.put(1, "Germany");
        states.put(2, "Spain");
        states.put(4, "France");
        states.put(3, "Italy");


        String first = states.get(2);
        System.out.println(first);
        //Получить набор всех ключей
        Set<Integer> keys = states.keySet();
        //Получить набор всех значений
        Collection<String> values = states.values();
        //Заменить элемент
        states.replace(1,"Poland");
        //Удаление элемента по ключу
        states.remove(2);

        for (Map.Entry<Integer, String> item : states.entrySet()) {
            System.out.printf("Key: %d Value: %s \n", item.getKey(), item.getValue());
        }

        Map<String, Person> people = new HashMap<String, Person>();
        people.put("1234i2", new Person("Tom"));
        people.put("1243i2", new Person("Bill"));
        people.put("1237i2", new Person("Nick"));
        for (Map.Entry<String, Person> item : people.entrySet()) {
            System.out.printf("Key: %d Value: %s \n", item.getKey(), item.getValue().getName());
        }




    }


}
