package lesson8.task2;

public enum DayWeeks {

    SUNDAY(1, "Воскресенье"), //Объекты
    MONDAY(2, "Понедельник"),
    TUESDAY(3, "Вторник"),
    WEDNESDAY(4, "Среда"),
    THURSDAY(5, "Четверг"),
    FRIDAY(6, "Пятница"),
    SATURDAY(7, "Суббота");

    int numberOfDayWeek; //Свойства
    String nameOfDayWeek;

    DayWeeks(int numberOfDayWeek, String nameOfDayWeek){   //Конструктор
        this.numberOfDayWeek = numberOfDayWeek;
        this.nameOfDayWeek = nameOfDayWeek;
    }

    public static String getValueOfDayWeekByNumber(int scannerValue) {   //Статичный метод, к которому можно обращаться только через класс (DayWeeks)
        DayWeeks [] dayWeeks = DayWeeks.values();
        for (int i = 0; i < dayWeeks.length; i++) {
            if (dayWeeks[i].numberOfDayWeek == scannerValue) {
                return dayWeeks[i].nameOfDayWeek;
            }
        }
        return null;
    }



}
