package lesson8.hwmonths;

public enum Months {

    JANUARY (1, "Январь"),
    FEBRUARY (2, "Февраль"),
    MARCH (3, "Март"),
    APRIL (4, "Апрель"),
    MAY (5, "Май"),
    JUNE (6, "Июнь"),
    JULY (7, "Июль"),
    AUGUST (8, "Август"),
    SEPTEMBER (9, "Сентябрь"),
    OCTOBER (10, "Октябрь"),
    NOVEMBER (11, "Ноябрь"),
    DECEMBER (12, "Декабрь");

    int monthNum;
    String monthName;

    Months(int monthNum, String monthName){
        this.monthNum = monthNum;
        this.monthName = monthName;
    }

    public static String printMonth(int scannerValue) {   //Статичный метод, к которому можно обращаться только через класс (DayWeeks)
        Months[] months = Months.values();
        for (Months month : months) {
            if (month.monthNum == scannerValue) {
                return month.monthName;
            }
        }
        return "Такого месяца не существует";
    }
}
