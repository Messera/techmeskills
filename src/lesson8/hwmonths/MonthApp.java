package lesson8.hwmonths;

import java.util.Scanner;

public class MonthApp {

    public static void main(String[] args) {
        System.out.println("Введите номер месяца:");

        Scanner scanner = new Scanner(System.in);

        System.out.println(Months.printMonth(scanner.nextInt()));
    }

}
