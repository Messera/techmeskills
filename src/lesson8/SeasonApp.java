package lesson8;

public class SeasonApp {
    public static void main(String[] args) {

        //Обращение к свойству объекта
        Season objSeason = Season.SUMMER;
        String result = objSeason.getSeasonValue();
        System.out.println(result);

        //Обращение к свойству возвращаемого объекта
        System.out.println(Season.AUTUMN.getSeasonValue());

    }
}
