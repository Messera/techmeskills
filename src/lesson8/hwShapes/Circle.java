package lesson8.hwShapes;

public class Circle extends Shape {

    protected int radius;

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void getArea() {
        System.out.println("Площадь круга равна: " + Math.PI * (radius * radius));
    }
}
