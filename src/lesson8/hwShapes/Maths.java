package lesson8.hwShapes;

import java.util.Scanner;

public class Maths {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Circle circle = new Circle();
        System.out.println("Введите радиус круга: ");
        circle.setRadius(scanner.nextInt());
        System.out.println("Радиус круга = " + circle.getRadius());
        circle.getArea();

        Rectangle rectangle = new Rectangle();
        System.out.println("Введите ширину прямоугольника: ");
        rectangle.setWidth(scanner.nextInt());
        System.out.println("Введите высоту прямоугольника: ");
        rectangle.setHeight(scanner.nextInt());
        System.out.println("У нас есть прямоугольник со сторонами: " + rectangle.getWidth() + " и " + rectangle.getHeight());
        rectangle.getArea();
        
    }
}
