package lesson8.hwShapes;

public class Rectangle extends Shape {

    private int width;
    private int height;

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getWidth() {
        return width;
    }

    @Override
    public void getArea() {
        System.out.println("Площадь прямоугольника равна: " + (width * height));
    }

}
