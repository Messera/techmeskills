package lesson8;

public enum Season {

    WINTER("Зима"),
    SPRING("Весна"),
    SUMMER("Лето"),
    AUTUMN("Осень");

    String seasonValue;

    Season (String seasonValue){
        this.seasonValue = seasonValue;
    }

    public String getSeasonValue(){
        return seasonValue;
    }

}
