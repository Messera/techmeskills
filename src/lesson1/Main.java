package lesson1;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        task1();
    }

// 1. Даны 2 числа. Вывести меньшее из них.
        private static void task1() {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Программа находит меньшее число");
            System.out.println("Введите а");
            double a = scanner.nextDouble();
            System.out.println("Введите b");
            double b = scanner.nextDouble();
            System.out.print("Используя цикл - ");

            if (a < b) {
                System.out.println(a);
            } else {
                System.out.println(b);
            }
            System.out.println("То же самое, используя функцию Math.min - " + Math.min(a, b));
        }

// 2. Даны 2 числа. Вывести их сумму.
        private static void task2() {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Программа выводит сумму двух чисел");
            System.out.println("Введите а");
            double a = scanner.nextDouble();
            System.out.println("Введите b");
            double b = scanner.nextDouble();
            System.out.println("Сумма чисел = " + (a + b));
        }

//3. Даны три числа целочисленных типов и с плавающей точкой. Вывести их произведение

    private static void task3() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Программа выводит произведение трех чисел");
        System.out.println("Введите целое число а");
        int a = scanner.nextInt();
        System.out.println("Введите дробное число b");
        double b = scanner.nextDouble();
        System.out.println("Введите дробное число c");
        double c = scanner.nextDouble();
        DecimalFormat decimalFormat = new DecimalFormat( "#.##" );
        String result = decimalFormat.format(a * b * c);
        System.out.println("Произведение трех чисел (с округлением до двух знаков после запятой) = " + result);
    }

//4.  Даны два числа. Вывести остаток от деления этих чисел

    private static void task4() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Программа выводит остаток от деления двух чисел");
        System.out.println("Введите а");
        double a = scanner.nextDouble();
        System.out.println("Введите b");
        double b = scanner.nextDouble();
        System.out.println("Остаток от деления a на b = " + (a % b));
    }
//5.  * Даны 2 числа с плавающей точкой, преобразовать их сумму к целочисленному значению

    private static void task5() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Программа выводит целочисленную сумму двух дробных чисел");
        System.out.println("Введите а");
        double a = scanner.nextDouble();
        System.out.println("Введите а");
        double b = scanner.nextDouble();
        System.out.println("Целочисленная сумма без математического округления = " + (int)(a + b));
        System.out.println("Целочисленная сумма c математическим округлением = " + Math.round(a + b));
    }

    }


