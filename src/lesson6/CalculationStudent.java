package lesson6;

import java.util.Random;

public class CalculationStudent {
    public static void main(String[] args) {
        hw6();
    }

    private static void cw6(String[] args) {

        String[] name = new String[]{"Саша", "Паша", "Даша", "Юля", "Наташа"};
        Student[] students = new Student[14];

        for (int i = 0; i < students.length; i++) {
            Student student = new Student(name[getRandom(4)], getRandom(4), getRandom(10));

            students[i] = student;
        }

        System.out.println("Вывод отличников ");
        for (int i = 0; i < students.length; i++) {
            students[i].getOnlyGoodStudent();
        }


        System.out.println("Вывод всех ");
        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i].toString());
        }


    }

    private static int getRandom(int maxValue) {
        Random random = new Random();
        return random.nextInt(maxValue);
    }


    private static void hw6() {

        String[] name = new String[]{"Саша", "Паша", "Даша", "Юля", "Наташа"};
        Student[] students = new Student[14];

        for (int i = 0; i < students.length; i++) {
            Student student = new Student(name[getRandom(4)], getRandom(4), getRandom(10));

            students[i] = student;
        }

        System.out.println("Вывод студентов 3 группы ");
        for (int i = 0; i < students.length; i++) {
            students[i].getOnlyOneGroup();
        }

        System.out.println("Вывод всех ");
        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i].toString());
        }



    }

}

