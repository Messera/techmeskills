package lesson6.cat;

public class Cat {

    String name;
    int age;
    int food;
    boolean satiety = false;

    Cat(String name, int age, int food, boolean satiety){
        this.name = name;
        this.age = age;
        this.food = food;
        this.satiety = satiety;
    }

    public void feeding(int gram) {
        this.food+= gram;
        if (this.food>50){
            this.satiety = true;
        }
        if (this.food>60) {
            System.out.println("");
            System.out.println("Кот обожрался.");
            System.out.println("");

        }
    }


    public String toString() {
        return "Имя кота: " + name + "\nВозраст кота: " + age + "\nКоличество корма в коте (в граммах): " + food + "\nСыт ли кот: " + satiety;
    }


}
