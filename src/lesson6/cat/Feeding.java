package lesson6.cat;

import java.util.Scanner;

public class Feeding {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Cat Blizzard = new Cat("Blizzard", 2, 10, false);

        System.out.println(Blizzard.toString());
        while (!Blizzard.satiety) {
            System.out.println("");
            System.out.println("Давайте покормим кота. \nСколько корма мы ему дадим?");
            Blizzard.feeding(scanner.nextInt());
            System.out.println(Blizzard.toString());
        }
    }

}
