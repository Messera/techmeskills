package lesson6;

public class Student {
    String name;
    int group;
    int grade;

    Student(){}

    Student(String name, int group, int grade){
        this.name = name;
        this.grade = grade;
        this.group = group;

    }

    public void setName(String name){
        this.name = name;
    }
    public void setGroup(int group) {
        this.group = group;
    }
    public void setGrade(int grade) {
        this.grade = grade;
    }


    public int getGrade(){              //почему не исп-м this?
        return grade;
    }
    public String getName(){
        return name;
    }
    public int getGroup(){
        return group;
    }

    @Override
    public String toString() {
        return "Имя: " + name + " Группа: " + group + " Оценка по диплому: " + grade;
    }

    public void getOnlyGoodStudent(){
        if (grade >= 9 ){
            System.out.println(toString());
        }
    }

    public void getOnlyOneGroup(){
        if (group == 3 ){
            System.out.println(toString());
        }
    }

}
