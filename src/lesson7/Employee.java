package lesson7;

public class Employee extends Person {

    protected String company;

    public Employee (String name, String surname, String company) {
        super(name, surname);
        this.company = company;
    }

    public String getCompany() {
        return company;
    }

    // WTF???
    @Override
    public void displayInfo() {
        System.out.println("Name: " + name + ", Surname: " + surname + ", Company: " + company);
    }

}
