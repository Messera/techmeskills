package lesson7;

public class PersonService {
    public static void main(String[] args) {
        Person person = new Person("David", "Caster");
        System.out.println("Person 1");
        System.out.println(person.getName() + "\n" + person.getSurname());

        Person person2 = new Person("David", "Caster");
        System.out.println("Person 2");
        System.out.println(person2.getName() + "\n" + person2.getSurname());

        Employee employee = new Employee("Kate", "Miller", "MTZ");
        System.out.println("Employee 1");
        employee.displayInfo();

        Employee employee2 = new Employee("Mary", "Right", "MTZ");
        System.out.println("Employee 2");
        System.out.println(employee2.getName() + "\n" + employee2.getSurname() + "\n" + employee2.getCompany());

        Employee employee3 = new Employee("July", "Stone", "MTZ");
        System.out.println("Employee 3");
        employee3.displayInfo();



    }

}
