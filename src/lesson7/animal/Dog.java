package lesson7.animal;

public class Dog extends Animal {

    protected double weight;

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void printVoice() {
        System.out.println("Гав!");
    }

    public String getVoice(){
        return "Гав";
    }


    public void printInfo() {
        System.out.println(animalID + " " + name + " " + dateOfBirth + " " + weight);
    }

}
