package lesson7.animal;

public class Cat extends Animal{

    protected String eyesColor;

    public void setEyesColor(String eyesColor){
        this.eyesColor = eyesColor;
    }

    public void printVoice() {
        System.out.println("Мяу!");
    }

    public String getVoice(){
        return "Мяу";
    }

    public void printInfo() {
        System.out.println(animalID + " " + name + " " + dateOfBirth + " " + eyesColor);
    }

}
