package lesson7.animal;

import java.util.Date;

public abstract class Animal {

    protected int animalID;
    protected String name;
    protected Date dateOfBirth;

    public  void setAnimalID(int animalID) {
        this.animalID = animalID;
    }
    public  void setName(String name) {
        this.name = name;
    }
    public  void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public abstract void printVoice();
    public abstract String getVoice();
    public abstract void printInfo();

}
