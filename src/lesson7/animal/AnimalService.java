package lesson7.animal;

import java.util.Date;

public class AnimalService {

    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.setAnimalID(1);
        cat.setName("Leo");

        Date date = new Date();
        cat.setDateOfBirth(date);
        cat.setEyesColor("Green");

        callPrint(cat);

        Dog dog = new Dog();
        dog.setAnimalID(2);
        dog.setName("Spike");
        dog.setDateOfBirth(date);
        dog.setWeight(14.5);

        callPrint(dog);

        Tiger tiger = new Tiger();
        tiger.setAnimalID(3);
        tiger.setName("Simba");
        tiger.setDateOfBirth(date);
        tiger.setEyesColor("Yellow");
        tiger.setCountEatenEmployers(9);
        callPrint(tiger);

    }

    private static void callPrint (Animal animal) {
        animal.printInfo();
        animal.printVoice();
        String voice = animal.getVoice();
        System.out.println(voice);
    }

}
