package lesson7.animal;

public class Tiger extends Cat {


    protected int countEatenEmployers;

    public void setCountEatenEmployers (int countEatenEmployers){
        this.countEatenEmployers = countEatenEmployers;
    }

    public void printVoice() {
        System.out.println("Р-р-р-р!");
    }

    public String getVoice(){
        return "Р-р-р-р";
    }

    public void printInfo() {
        System.out.println(animalID + " " + name + " " + dateOfBirth + " " + eyesColor + " " + countEatenEmployers);
    }


}
