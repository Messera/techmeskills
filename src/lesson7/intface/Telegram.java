package lesson7.intface;

public class Telegram  implements Messenger {

    public void sendMessage() {
        System.out.println("Отправил сообщение в Телеграм");
    }

    public void getMessage() {
        System.out.println("Получил сообщение в Телеграме");
    }
}
