package lesson7.intface;

public class WhatsApp implements Messenger {

    public void sendMessage() {
        System.out.println("Отправил сообщение в Вотсап");
    }

    public void getMessage() {
        System.out.println("Получил сообщение в Вотсап");
    }

}
