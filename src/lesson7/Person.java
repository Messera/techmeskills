package lesson7;

public class Person {

    protected String name;
    protected String surname;

    public Person(String next) {
    }

    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name == null ? "Name not found" : name;
    }

    public String getSurname() {
        return surname == null ? "Surname not found" : surname;
    }

    public void displayInfo() {
        System.out.println("Name " + name + "Surname " + surname);
    }

}
