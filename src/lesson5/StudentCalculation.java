package lesson5;

import java.util.Random;

public class StudentCalculation {

    public static void main(String[] args) {

        task2();

    }

    private static void task1() {
        String[] name = new String[]{"Саша", "Паша", "Даша", "Юля", "Наташа"};
        Student[] students = new Student[14];
        for (int i = 0; i < students.length; i++) {

            Student student = new Student();
            student.setGrade(getRandom(5));
            student.setName(name[getRandom(4)]);
            student.setGroup(getRandom(3));
            students[i] = student;
        }

        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i].toString());
        }
    }

    private static int getRandom(int maxLimit) {
        Random random = new Random();
        return random.nextInt(maxLimit);
    }


    private static void task2() {


        Student[] students = new Student[3];
        // for (int i = 0; i < students.length; i++) {


        Student newStudent1 = new Student("Kate", 3, getRandom(10));
        Student newStudent2 = new Student("Paul", 5, getRandom(10));
        Student newStudent3 = new Student("Tom", 1, getRandom(10));
        students[0] = newStudent1;
        students[1] = newStudent2;
        students[2] = newStudent3;
        //   }

        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i].toString());

        }
    }

}

