package lesson5.hw;

import java.util.Random;

public class Users {


    private static int getRandom(int maxLimit) {
        Random random = new Random();
        return random.nextInt(maxLimit);
    }

    public static void main(String[] args) {
        task2();
    }

    private static void task1() {

        String[] name = new String[]{"Jack", "Lucky", "Beauty", "Katiara", "seltaeB"};
        User[] users = new User[10];
        for (int i = 0; i < users.length; i++) {

            User user = new User();
            user.setName(name[getRandom(5)]);
            user.getPassword(getRandom(100000000));
            users[i] = user;
        }

        for (int i = 0; i < users.length; i++) {
            System.out.println(users[i].toString());
        }
    }

    private static void task2() {

        String[] name = new String[]{"Jack", "Lucky", "Beauty", "Katiara", "seltaeB"};
        User[] users = new User[5];
        for (int i = 0; i < users.length; i++) {
            User newUser = new User(name[getRandom(5)], getRandom(100000000));
            users[i] = newUser;
        }

        for (int i = 0; i < users.length; i++) {
            System.out.println(users[i].toString());

        }

    }

}
