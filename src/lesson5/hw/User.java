package lesson5.hw;

public class User {

    public String name;
    public int password;

    User() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void getPassword(int password) {
        this.password = password;
    }


    public String toString() {
        return "Name: " + name + ", password: " + password;
    }

    ///////

    User(String name, int password) {
        this.name = name;
        this.password = password;
    }

}
