package lesson12;

import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class SetOfFruits {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Set<String> fruits = new LinkedHashSet<>();

        for (int i = 0; i < 5; i++) {
            fruits.add(scanner.next());
        }

        printAllFruits(fruits);

        System.out.println("Если хотите удалить элемент - введите да. Если не хотите - введите нет.");
        String decision = scanner.next();
        while (!decision.equals("нет")) {
            if (decision.equals("да")) {
                System.out.println("Введите элемент, который хотите удалить: ");
                fruits.remove(scanner.next());
            }

            printAllFruits(fruits);
            System.out.println("Если хотите удалить еще элемент - введите да. Если не хотите - введите нет.");
            decision = scanner.next();

        }
    }

    static void printAllFruits(Set<String> fruits) {
        for (String fruit : fruits) {
            System.out.println(fruit);
        }
    }

}



