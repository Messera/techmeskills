package lesson12;

import java.util.*;

public class MapOfFruits {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Map<String, String> fruits = new HashMap();

        System.out.println("Введите парами вид и тип (арбуз - ягода)");

        for (int i = 0; i < 5; i++) {
            String name = scanner.next();
            String type = scanner.next();
            fruits.put(name, type);
        }

        printAllFruits(fruits);

    }


    static void printAllFruits(Map<String, String> fruits) {
        for (Map.Entry fruit : fruits.entrySet()) {
           // System.out.println(fruit);
            StringBuilder builder = new StringBuilder();
            System.out.println(builder.append(fruit.getKey()).append(" - ").append(fruit.getValue()));
        }
    }

}
