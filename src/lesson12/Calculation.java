package lesson12;

import java.util.Scanner;

public class Calculation {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите делимое: ");
        int tmp = scanner.nextInt();
        System.out.println("Введите делитель: ");
        int zero = scanner.nextInt();

        try {
            System.out.println("Результат деления: " + tmp / zero);
        } catch (ArithmeticException ex) {
            System.out.println("На ноль делить нельзя. Введите другое число:");
            zero = scanner.nextInt();
            try {
                System.out.println("Результат деления: " + tmp / zero);

            } catch (ArithmeticException ex2){
                System.out.println("На ноль делить нельзя.");

            }
        }
    }

}
