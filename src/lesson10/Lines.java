package lesson10;

import java.util.Scanner;

public class Lines {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите количество строк: ");
        String[] arr = new String[scanner.nextInt()];

        System.out.println("Заполните строки значениями: ");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.next();
        }

        String ShortestLine = arr[0];
        String LongestLine = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i].length() < ShortestLine.length()) {
                ShortestLine = arr[i];
            }
            if (arr[i].length() > LongestLine.length()) {
                LongestLine = arr[i];
            }
        }
        System.out.println("Самая короткая строка: '" + ShortestLine + "' имеет длину " + ShortestLine.length() +
                ". Самая длинная строка: '" + LongestLine + "' имеет длину " + LongestLine.length());

    }
}
