package lesson10.hw2;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class ListMin {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите элементы массива. Чтобы прекратить ввод напишите STOP");
        List<Integer> numbers = new LinkedList<>();
         for (int i = 0; ; i++) {
             String n = scanner.next();
             if (n.equals("STOP"))
                 break;
            numbers.add(i, Integer.valueOf(n));

        }
        int minValue = numbers.get(0);
        for (Integer number : numbers) {
            if (number < minValue) {
                minValue = number;
            }
        }
        System.out.println(minValue);
    }

}
