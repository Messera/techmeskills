package lesson10;

import java.util.LinkedList;
import java.util.List;

public class ListByArray {

    public static void main(String[] args) {

        List<Integer> numbers = new LinkedList<>();
        numbers.add(9);
        numbers.add(3);
        numbers.add(1);
        numbers.add(2);
        int maxValue = numbers.get(0);
        for (Integer number : numbers) {
                if (number > maxValue) {
                    maxValue = number;
                }
            }
        System.out.println(maxValue);
    }
}
