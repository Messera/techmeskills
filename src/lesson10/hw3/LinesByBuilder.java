package lesson10.hw3;

import java.util.Scanner;

public class LinesByBuilder {

//    3) Ввести n строк с консоли. Вывести на консоль те строки, длина которых больше средней, а также длину.
//    для вывода результат используйте StringBuilder

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите количество строк: ");
        int n = scanner.nextInt();
        String[] arr = new String[n];

        System.out.println("Заполните строки значениями: ");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.next();
        }

        int length = 0;
        int avg = 0;
        for (int i = 0; i < arr.length; i++) {
            length += arr[i].length();
            }
        avg = length/n;

        StringBuilder result = new StringBuilder();

        System.out.println("Средняя длина строки: " + avg + "\nВывожу строки, у которых длина больше средней: ");


        for (int i = 0; i < arr.length; i++) {
            if(arr[i].length() > avg) {
                result = result.append("Строка ").append(i).append(" - ").append(arr[i]).append(", и её длина: ").append(arr[i].length()).append("\n");
        }
        }
        System.out.println(result);
    }

}
