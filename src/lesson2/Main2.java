package lesson2;

import java.util.Scanner;

public class Main2 {

    public static void main(String[] args) {task04();}

        private static void task1(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите а");
        int a = scanner.nextInt();
        if (a>0) {
            System.out.println(a+1);
        } else if (a<0) {
            System.out.println(a-2);
        } else {
            a = 10;
            System.out.println(a);
        }
    }

    private static void task2() {
        Scanner scanner = new Scanner(System.in);
        switch (scanner.nextInt()) {
            case 1:
                System.out.println("Январь");
                break;
            case 2:
                System.out.println("Февраль");
                break;
            case 3:
                System.out.println("Март");
                break;
            case 4:
                System.out.println("Апрель");
                break;
            case 5:
                System.out.println("Май");
                break;
            case 6:
                System.out.println("Июнь");
                break;
            case 7:
                System.out.println("Июль");
                break;
            case 8:
                System.out.println("Август");
                break;
            case 9:
                System.out.println("Сентябрь");
                break;
            case 10:
                System.out.println("Октябрь");
                break;
            case 11:
                System.out.println("Ноябрь");
                break;
            case 12:
                System.out.println("Декабрь");
                break;
            default:
                System.out.println("Такого месяца не бывает :)");
                break;

        }

    }

    private static void task0(){
/*
        int result = 0;
        for (int i = 10; i <= 55; i = i + 2) {
            result = result + i;
        }
        System.out.println(result);
    }


    int result = 0;
        for(
    int i = 10;
    i<=55;i++)

    {
        if (i % 2 == 0) {
            result = result + i;
        }
        System.out.println(result);
    }

*/
        int i = 10;
        int result = 0;
        while (i <= 55) {
            if (i % 2 == 0) {
                result = result + i;
            }
            i++;
        }
        System.out.println(result);
    }

    private static void task3(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Программа покажет, сколько положительных чисел введено");
        System.out.println("Введите а, b и с");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int i = 0;
        if (a>0) {
            i++;
        }
        if (b>0) {
            i++;
        }
        if (c>0) {
            i++;
        }
        System.out.println("Введено положительных чисел: " +i);

    }

    private static void task4() {

        System.out.println("Программа покажет, сколько положительных и отрицательных чисел введено");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите а, b и с");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int pos = 0;
        int neg = 0;
        if (a > 0) {
            pos++;
        } else if (a < 0) {
            neg++;
        }
        if (b > 0) {
            pos++;
        } else if (b < 0) {
            neg++;
        }
        if (c > 0) {
            pos++;
        } else if (c < 0) {
            neg++;
        }
        System.out.println("Введено положительных чисел: " + pos + ". Введено отрицательных чисел: " + neg);
    }



    private static void task01(){
        for (int i=1;i<=10;i++) {
            switch (i){
                case 1: System.out.println("Один");
                    break;
                case 2: System.out.println("Два");
                    break;
                case 3: System.out.println("Три");
                    break;
                case 4: System.out.println("Четыре");
                    break;
                case 5: System.out.println("Пять");
                    break;
                case 6: System.out.println("Шесть");
                    break;
                case 7: System.out.println("Семь");
                    break;
                case 8: System.out.println("Восемь");
                    break;
                case 9: System.out.println("Девять");
                    break;
                case 10: System.out.println("Десять");
                    break;
            }
        }
    }

    private static void task02(){
        int[] arr = new int[]{154,32,53,48,5965,36,75,82,912};
        for (int j : arr) {
            System.out.println(j);

            /*
        int[] arr = new int[]{1,2,3,4,5,6,7,8,9};
        for (int i=0; i< arr.length; i++){
        System.out.println(arr[i]);
             */

        }
    }

    private static void task03(){

        int[] arr = new int[]{-1433332, -253, -343232, -434, -554, -63, -547, -48, -9};
        int a = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > a) {
                a = arr[i];
            }
        }
        System.out.println("Максимальное значение массива: " + a);

    }

    private static void task04(){

        int[] arr = new int[10];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 100);
            System.out.println(arr[i]);
        }
        int a = arr[0];
        for (int j = 1; j < arr.length; j++) {
            if (arr[j] > a) {
                a = arr[j];
            }
        }
        System.out.println("Максимальное значение массива: " + a);

    }




}
