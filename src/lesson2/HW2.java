package lesson2;

import java.util.Scanner;

public class HW2 {
    public static void main(String[] args) {
        task4();
    }

    private static void task1() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Программа покажет, сколько положительных чисел введено");
        System.out.println("Введите а, b и с");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int i = 0;
        if (a > 0) {
            i++;
        }
        if (b > 0) {
            i++;
        }
        if (c > 0) {
            i++;
        }
        System.out.println("Введено положительных чисел: " + i);

    }

    private static void task2() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Программа покажет, сколько положительных и отрицательных чисел введено");
        System.out.println("Введите а, b и с");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int pos = 0;
        int neg = 0;
        if (a > 0) {
            pos++;
        } else if (a < 0) {
            neg++;
        }
        if (b > 0) {
            pos++;
        } else if (b < 0) {
            neg++;
        }
        if (c > 0) {
            pos++;
        } else if (c < 0) {
            neg++;
        }
        System.out.println("Введено положительных чисел: " + pos + ". Введено отрицательных чисел: " + neg);
    }

    private static void task3() {

        int[] arr = new int[10];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 100);
            System.out.println(arr[i]);
        }
        int a = arr[0];
        for (int j = 1; j < arr.length; j++) {
            if (arr[j] < a) {
                a = arr[j];
            }
        }
        System.out.println("Минимальное значение массива: " + a);

    }

    private static void task4() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество программистов (при введении числа, юный падаван, помни - программисты учитываются только живые и полноценные физически):");
        double b = scanner.nextDouble();

        if (b % 1 != 0) {
            System.out.println("Прекондишены читать ты должен внимательнее, юный падаван!");
            return;
        }

        int a = (int) b;

        if (a < 0) {
            System.out.println("Прекондишены читать ты должен внимательнее, юный падаван!");
            return;
        }

        if (a == 0) {
            System.out.println("Верховный канцлер Палпатин! Приказ 66 исполнен! Все джедаи уничтожены!");
            return;
        }

        if (a % 10 == 1 && a % 100 != 11) {
            System.out.println("Теперь у нас есть " + a + " программист");
        }

        if (a % 10 == 0) {
            System.out.println("Теперь у нас есть " + a + " программистов");
        }

        if (a == 2 || a == 3 || a == 4 || (a > 20 && a / 10 % 10 != 1 && (a % 10 == 2 || a % 10 == 3 || a % 10 == 4))) {
            System.out.println("Теперь у нас есть " + a + " программиста");
        }

        int[] intArray = new int[]{5, 6, 7, 8, 9, 11, 12, 13, 14};
        for (int x : intArray) {
            if (x == a || (a % 10 == x || a % 100 == x)) {
                System.out.println("Теперь у нас есть " + a + " программистов");
            }
        }


    }
}


